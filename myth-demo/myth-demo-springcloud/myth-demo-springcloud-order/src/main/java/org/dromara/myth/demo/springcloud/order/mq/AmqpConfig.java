package org.dromara.myth.demo.springcloud.order.mq;

import org.dromara.myth.common.config.MythConfig;
import org.dromara.myth.common.config.MythDbConfig;
import org.dromara.myth.core.bootstrap.MythTransactionBootstrap;
import org.dromara.myth.core.service.MythInitService;
import org.dromara.myth.core.service.MythMqSendService;
import org.dromara.myth.rabbitmq.service.RabbitmqSendServiceImpl;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Amqp config.
 * @author xiaoyu
 */
@Configuration
public class AmqpConfig {

    @Bean
    public MythTransactionBootstrap mythTransactionBootstrap() {
        MythDbConfig mythDbConfig = new MythDbConfig();
        mythDbConfig.setUrl("jdbc:mysql://192.168.100.54:3306/myth?useUnicode=true&amp;characterEncoding=utf8");
        mythDbConfig.setDriverClassName("com.mysql.jdbc.Driver");
        mythDbConfig.setUsername("root");
        mythDbConfig.setPassword("Qq_274666893");

        MythInitService mythInitService = new MythInitService() {
            @Override
            public void initialization(MythConfig mythConfig) {
                mythConfig.setMythDbConfig(mythDbConfig);
                mythConfig.setSerializer("kryo");
                mythConfig.setRepositorySuffix("order-service");
                mythConfig.setRepositorySupport("db");
            }
        };
        MythTransactionBootstrap bootstrap = new MythTransactionBootstrap(mythInitService);

        bootstrap.setMythDbConfig(mythDbConfig);

        return bootstrap;
    }

    /**
     * My container factory simple rabbit listener container factory.
     *
     * @param configurer        the configurer
     * @param connectionFactory the connection factory
     * @return the simple rabbit listener container factory
     */
    @Bean
    @ConditionalOnProperty(prefix = "spring.rabbitmq", name = "host")
    public SimpleRabbitListenerContainerFactory myContainerFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setPrefetchCount(100);
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    /**
     * Inventory queue queue.
     *
     * @return the queue
     */
    @Bean
    public Queue accountQueue() {
        return new Queue("account");
    }
    @Bean
    public Queue inventoryQueue() {
        return new Queue("inventory");
    }

    // 创建交换机
//    @Bean
//    public FanoutExchange defFanoutExchange() {
//        return new FanoutExchange(TEST_FANOUT_EXCHANGE);
//    }
    @Bean
    DirectExchange directExchange() {
        return new DirectExchange("directExchange");
    }

    // 队列与交换机进行绑定
    @Bean
    Binding bindingDirect1() {
        return BindingBuilder.bind(accountQueue()).to(directExchange()).with("account");
    }
    //队列与交换机绑定并添加路由key（direct和topic模式）
    @Bean
    Binding bindingDirect2() {
        return BindingBuilder.bind(inventoryQueue()).to(directExchange()).with("inventory");
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setExchange("directExchange");
        return rabbitTemplate;
    }

//    @Bean
//    public MythMqSendService rabbitmqSendService(ConnectionFactory connectionFactory) {
//        RabbitmqSendServiceImpl rabbitmqSendService = new RabbitmqSendServiceImpl();
////        rabbitmqSendService.setAmqpTemplate(rabbitTemplate(connectionFactory));
//        return rabbitmqSendService;
//    }
}